package com.example.rajeev.pizzacorner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity {
    String pass="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

   public void orderPizza(View v) {

      /* final AlertDialog.Builder ad = new AlertDialog.Builder(this);

       final String items[]={"TYPE","VEG","NON-VEG"};
       ad.setIcon(android.R.drawable.btn_star_big_on);
       ad.setTitle("Select Your Option");

       //create array adapter to add list to spinner
       ArrayAdapter aa= new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,items);
       Spinner spin=new Spinner(this);
       spin.setAdapter(aa); //add adapter to spinner
       ad.setView(spin); //add spinner to alert dialog box



       spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              // Toast.makeText(MainActivity.this, ""+items[position], Toast.LENGTH_SHORT).show();
              String type=""+items[position];


            if(type.equals("VEG"))
               {
                   pass="VEG";
                   //Toast.makeText(MainActivity.this, "VEG", Toast.LENGTH_SHORT).show();


               }
               else if(type.equals("NON-VEG"))
               {
                   pass="NON-VEG";
                   //Toast.makeText(MainActivity.this, "NON-VEG", Toast.LENGTH_SHORT).show();
                  // ad.setCancelable(true);
               }
                else
                      {
                          pass="";
                      }
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });

       ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {

              if(pass.equals("VEG"))
              {
                  Toast.makeText(MainActivity.this, "VEG", Toast.LENGTH_SHORT).show();
              }
               else if(pass.equals("NON-VEG"))
              {
                  Toast.makeText(MainActivity.this, "NON-VEG", Toast.LENGTH_SHORT).show();
              }
               else
              {
                  Toast.makeText(MainActivity.this, "Please Select Any one TYPE (VEG / NON-VEG)", Toast.LENGTH_SHORT).show();
              }
           }
       });
       ad.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {
               dialog.cancel();
           }
       });


       ad.setCancelable(false);

       ad.show();
*/
       startActivity(new Intent(this,VegPizzaSizeList.class));

   }
    public void orderTrack(View v)
    {
        startActivity(new Intent(this,VegPizzaName.class));

        Toast.makeText(MainActivity.this, "Under Development ....", Toast.LENGTH_SHORT).show();
    }
}


